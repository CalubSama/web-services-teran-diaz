import React from 'react';
import {ScrollView } from 'react-native';
import SingleView from './SingleView.js';

const App = () => {
  return (
    <>
      <ScrollView>
        <SingleView></SingleView>
      </ScrollView>
    </>
  )
}

export default App;
